@extends('adminlte::page')

@section('title', ' Expenses | Lançamentos')

@section('content_header')
    <h1>Lançamentos</h1>
@stop

@section('content')
    
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">            
            <div class="box-body">
                <lancamento-component></lancamento-component>
            </div>
        </div>
    </div>
</div>

@stop