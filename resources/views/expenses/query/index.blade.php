@extends('adminlte::page')

@section('title', ' Expenses | Consulta de Despesas')

@section('content_header')
    <h1>Aprovação / Consulta</h1>
@stop

@section('content')
    
<div class="row">
    <div class="col-md-12">
        
        <div class="box box-danger">
            <div class="box-header with-border">
                <h2 class="box-title">Pendentes</h2>                
            </div>
            <div class="box-body">                
                <table class="table table-striped table-condensed">
                    <thead>
                        <tr>
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Centro de Custo</th>
                            <th class="text-center">Detalhes</th>
                            <th style="width: 100px; text-align: center">Aprovar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($expenses_pend as $expense)
                        <tr>
                            <td>{{ $expense->user->name }}</td>
                            <td>{{Carbon\Carbon::parse($expense->date)->format('d/m/Y')}}</td>
                            <td>{{ $expense->CentersCost->description }}</td>
                            <td class="text-center">
                                <button class="btn btn-default open-detail" data-id="{{ $expense->id }}"><i class="fa fa-plus"></i></button>
                            </td>
                            <td class="text-center">
                                <a class="btn btn-default" href="{{ route('aprove', compact('expense')) }}"
                                onclick="event.preventDefault();if(confirm('Deseja aprovar a despesa?')){document.getElementById('form-aprove{{ $expense->id }}').submit();}"><i class="fa fa-thumbs-up"></i></a>
                                <form id="form-aprove{{ $expense->id }}" style="display: none" action="{{ route('aprove', compact('expense')) }}" method="POST">
                                    {{csrf_field()}}
                                    {{method_field('PUT')}}
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>


        <div class="box box-success">
            <div class="box-header with-border">
                <h2 class="box-title">Aprovados</h2>                
            </div>
            <div class="box-body">
                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Usuário</th>
                            <th>Data</th>
                            <th>Centro de Custo</th>
                            <th class="text-center">Detalhes</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($expenses_aprov as $expense)
                        <tr>
                            <td>{{ $expense->user->name }}</td>
                            <td>{{Carbon\Carbon::parse($expense->date)->format('d/m/Y')}}</td>
                            <td>{{ $expense->CentersCost->description }}</td>
                            <td class="text-center">
                                <button class="btn btn-default open-detail" data-id="{{ $expense->id }}"><i class="fa fa-plus"></i></button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>        
            </div>
        </div>

        <div class="modal fade" id="modal-default">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                      <h4 class="modal-title">Detalhes da despesa</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped table-condensed" id="table-expenses">
                            <thead>
                                <tr>
                                    <th>Despesa</th>
                                    <th>Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Sair</button>
                    </div>
                  </div>
                  <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
              </div>
    </div>
</div>

@stop

@section('adminlte_js')
    <script>
        $(function() {
            // import axios from 'axios':
            $(document).on('click', '.open-detail', function () {
                $('#table-expenses tbody').html('')                
                
                $("#modal-default").modal('show')
                let id = $(this).data('id');
                axios.get(`/despesas/detalhes/${id}`).then(({data}) => {
                    let items = data;
                    let html = '';
                    for(let i in items) {
                        html += `<tr><td>${items[i].description}</td><td>R$ ${parseFloat(items[i].valor).toFixed(2)}</td></tr>`;
                    }
                    $('#table-expenses tbody').html(html)
                })
            })
        })
    </script>

@stop
