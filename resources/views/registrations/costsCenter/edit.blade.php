@extends('adminlte::page')

@section('title', ' Expenses | Editar Centro de Custo')

@section('content_header')
    <h1>Editar Centro de Custo</h1>
@stop

@section('content')
    
<div class="row">
        <div class="col-md-12">
            @if($errors->any())
                <div class="panel panel-danger">
                    <div style="padding: 3px 15px;" class="panel-heading">{{ count($errors->all()) == 1 ? "Ocorreu um erro ao gravar!" : "Ocorreram alguns erros ao gravar!" }}</div>
            
                    <ul class="list-group" >
                        @foreach($errors->all() as $error)
                            <li class="list-group-item" style="padding: 2px 15px;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>        
            @endif
            
            <form action="{{ route('centro-de-custos.update', compact('centro_de_custo')) }}" method="post">
                {{method_field('PUT')}}
                {{ csrf_field() }}
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <label>Descrição</label>
                                <input type="text" name="description" id="description" class="form-control"  autofocus value="{{ old('description') ?? $centro_de_custo->description }}" >
                            </div>
                            <div class="form-group col-md-6">
                                <label>Usuário aprovados</label>
                                <select name="user_id" id="user_id" class="form-control">
                                    <option value="">Selecione</option>
                                    @forelse($users as $u)
                                        <option  {{ old('user_id') ?? $centro_de_custo->user_id == $u->id ? 'selected' : ''}}  value="{{ $u->id }}">{{ $u->name }}</option>
                                    @empty
                                        <option value="">Usuários não encontrados</option>
                                    @endforelse
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="box-header">
                        <a href="{{ route('centro-de-custos.index') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Voltar</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@stop

@section('js')
<script>

        $(document).ready( function () {
        
        })
        
</script>
@stop
