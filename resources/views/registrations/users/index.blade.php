@extends('adminlte::page')

@section('title', ' Expenses | Usuários')

@section('content_header')
    <h1>Usuários</h1>
@stop

@section('content')
    
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-hover table-striped" id="table">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Admin</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($users as $u) 
                            <tr>
                                <td>{{$u->name}}</td>
                                <td>{{$u->email}}</td>
                                <td>
                                @if($u->isAdmin == 1)
                                    <a class="btn btn-success" href="{{ route('usuarios.setAdmin', compact('u')) }}"
                                    onclick="event.preventDefault();if(confirm('Deseja atribuir permissão de administrador ao usuário?')){document.getElementById('form-admin{{ $u->id }}').submit();}"><i class="fa fa-thumbs-up"></i></a>
                                    <form id="form-admin{{ $u->id }}" style="display: none" action="{{ route('usuarios.setAdmin', compact('u')) }}" method="POST">
                                        {{csrf_field()}}
                                        {{method_field('PUT')}}
                                    </form>
                                @else
                                    <a class="btn btn-danger" href="{{ route('usuarios.setAdmin', compact('u')) }}"
                                    onclick="event.preventDefault();if(confirm('Deseja atribuir permissão de administrador ao usuário?')){document.getElementById('form-admin{{ $u->id }}').submit();}"><i class="fa fa-thumbs-down"></i></a>
                                    <form id="form-admin{{ $u->id }}" style="display: none" action="{{ route('usuarios.setAdmin', compact('u')) }}" method="POST">
                                        {{csrf_field()}}
                                        {{method_field('PUT')}}
                                    </form>
                                @endif
                                </td>
                            </tr>
                        @empty 
                            <tr>
                                <td colspan='3' class='text-center'>Informações não encontradas!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script>

        $(document).ready( function () {
        
            $('#table').DataTable({
                "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
        
        })
        
</script>
@stop
