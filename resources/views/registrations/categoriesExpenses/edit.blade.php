@extends('adminlte::page')

@section('title', ' Expenses | Editar Categoria de Despesa')

@section('content_header')
    <h1>Editar Categoria de Despesa</h1>
@stop

@section('content')
    
<div class="row">
        <div class="col-md-12">
            @if($errors->any())
                <div class="panel panel-danger">
                    <div style="padding: 3px 15px;" class="panel-heading">{{ count($errors->all()) == 1 ? "Ocorreu um erro ao gravar!" : "Ocorreram alguns erros ao gravar!" }}</div>
            
                    <ul class="list-group" >
                        @foreach($errors->all() as $error)
                            <li class="list-group-item" style="padding: 2px 15px;">{{$error}}</li>
                        @endforeach
                    </ul>
                </div>        
            @endif
            
            <form action="{{ route('categoria-de-despesas.update', compact('categoria_de_despesa')) }}" method="post">
                {{method_field('PUT')}}
                {{ csrf_field() }}
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label>Descrição</label>
                                <input type="text" name="description" id="description" class="form-control"  autofocus value="{{ old('description') ?? $categoria_de_despesa->description }}" >
                            </div>                          
                        </div>
                    </div>
                    <div class="box-header">
                        <a href="{{ route('categoria-de-despesas.index') }}" class="btn btn-default"><i class="fa fa-arrow-left"></i> Voltar</a>
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    
@stop

@section('js')
<script>

        $(document).ready( function () {
        
        })
        
</script>
@stop
