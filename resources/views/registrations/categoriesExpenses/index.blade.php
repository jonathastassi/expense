@extends('adminlte::page')

@section('title', ' Expenses | Categorias de Despesa')

@section('content_header')
    <h1>Categorias de Despesa</h1>
@stop

@section('content')
    
<div class="row">
    <div class="col-md-12">

        @if($errors->any())
            <div class="panel panel-danger">
                <div style="padding: 3px 15px;" class="panel-heading">{{ count($errors->all()) == 1 ? "Ocorreu um erro!" : "Ocorreram alguns erros!" }}</div>
        
                <ul class="list-group" >
                    @foreach($errors->all() as $error)
                        <li class="list-group-item" style="padding: 2px 15px;">{{$error}}</li>
                    @endforeach
                </ul>
            </div>        
        @endif
        
        <div class="box box-primary">
            <div class="box-header with-border">
                <a href="{{ route('categoria-de-despesas.create') }}" class="btn btn-success pull-left"><i class="fa fa-plus" aria-hidden="true"></i>
                    Novo
                </a>
            </div>
            <div class="box-body">
                <table class="table table-hover table-striped" id="table">
                    <thead>
                        <tr>
                            <th>Descrição</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($categoryExpenses as $c) 
                            <tr>
                                <td>{{$c->description}}</td>
                                <td style="width: 100px;">
                                    <a href="{{ route('categoria-de-despesas.edit', compact('c')) }}" class="btn btn-default"><i class="fa fa-pencil"></i></a>
                                    <a class="btn btn-default" href="{{ route('categoria-de-despesas.destroy', compact('c')) }}"
                                        onclick="event.preventDefault();if(confirm('Tem certeza que deseja excluir?')){document.getElementById('form-delete{{ $c->id }}').submit();}"><i class="fa fa-trash"></i></a>
                                    <form id="form-delete{{ $c->id }}" style="display: none" action="{{ route('categoria-de-despesas.destroy', compact('c')) }}" method="POST">
                                        {{csrf_field()}}
                                        {{method_field('DELETE')}}
                                    </form>
                                </td>
                            </tr>
                        @empty 
                            <tr>
                                <td colspan='4' class='text-center'>Informações não encontradas!</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@stop

@section('js')
<script>

        $(document).ready( function () {
        
            $('#table').DataTable({
                "language": {
                    "sEmptyTable": "Nenhum registro encontrado",
                    "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                    "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                    "sInfoPostFix": "",
                    "sInfoThousands": ".",
                    "sLengthMenu": "_MENU_ resultados por página",
                    "sLoadingRecords": "Carregando...",
                    "sProcessing": "Processando...",
                    "sZeroRecords": "Nenhum registro encontrado",
                    "sSearch": "Pesquisar",
                    "oPaginate": {
                        "sNext": "Próximo",
                        "sPrevious": "Anterior",
                        "sFirst": "Primeiro",
                        "sLast": "Último"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar colunas de forma ascendente",
                        "sSortDescending": ": Ordenar colunas de forma descendente"
                    }
                }
            });
        
        })
        
</script>
@stop
