<?php

Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', 'HomeController@index');    
    Route::get('/', 'HomeController@index');    
});

Auth::routes();

Route::group(['name' => 'cadastros', 'prefix' => 'cadastros'], function () {
    Route::group(['middleware' => 'auth'], function() {
        Route::put('/usuarios/{usuario}/setAdmin', 'UsersController@setAdmin')->name('usuarios.setAdmin');
        Route::resource('usuarios', 'UsersController');

        Route::get('/centro-de-custos/list', 'CentersCostController@list')->name('centro-de-custos.list');
        Route::resource('centro-de-custos', 'CentersCostController');

        Route::get('/categoria-de-despesas/list', 'CategoryExpensesController@list')->name('categoria-de-despesas.list');        
        Route::resource('categoria-de-despesas', 'CategoryExpensesController');
    });
});

Route::group(['name' => 'despesas', 'prefix' => 'despesas'], function () {
    Route::group(['middleware' => 'auth'], function() {
        Route::put('/{expense}/aprovar', 'ExpenseController@aprove')->name('aprove');
        Route::get('/detalhes/{id}', 'ExpenseController@detail')->name('detail');
        Route::get('/lancamentos', 'ExpenseController@launch')->name('launch');
        Route::post('/store', 'ExpenseController@store')->name('store');
        Route::get('/consulta', 'ExpenseController@query')->name('query');
    });
});