<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryExpenses extends Model
{
    protected $fillable = ['description'];    
}
