<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model
{
    protected $fillable = ['date', 'centercost_id', 'pendente', 'user_id', 'useraprova_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function CentersCost()
    {
        return $this->belongsTo('App\CentersCost', 'centercost_id');
    }
}
