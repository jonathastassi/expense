<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpenseItem extends Model
{
    protected $fillable = ['valor', 'category_expenses_id', 'expenses_id'];

}
