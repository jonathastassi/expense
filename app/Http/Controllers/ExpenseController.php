<?php

namespace App\Http\Controllers;

use Auth;
use App\Expense;
use App\ExpenseItem;
use App\CentersCost;
use Illuminate\Http\Request;

class ExpenseController extends Controller
{
    public function launch() 
    {
        $centersCosts = CentersCost::orderBy('description')->get();

        return view('expenses.launch.index', compact('centersCosts'));     
    }

    public function store() 
    {
        $expense = new Expense();
        $expense->date = Request()->date;
        $expense->centercost_id = Request()->centercost;
        $expense->user_id = Auth::user()->id;
        $expense->save();

        foreach(Request()->list as $item) {
            $expenseItem = new ExpenseItem();
            $expenseItem->valor = floatval($item['value']);
            $expenseItem->category_expenses_id = $item['categoryexpense_id'];
            $expenseItem->expenses_id = $expense->id;
            $expenseItem->save();
        }

        return json_encode([
            'status' => true
        ]);
    }

    public function query() 
    {
        $user = Auth::user()->id;
        $expenses_pend = Expense::join('centers_costs', 'expenses.centercost_id', '=', 'centers_costs.id')
        ->where('centers_costs.user_id', $user )
        ->where('expenses.pendente', 1 )
        ->orderBy('expenses.date')        
        ->get([
            'expenses.*',            
        ]);
        
        $expenses_aprov = Expense::join('centers_costs', 'expenses.centercost_id', '=', 'centers_costs.id')
        ->where('centers_costs.user_id', $user)
        ->where('expenses.pendente', 0 )
        ->orderBy('expenses.date')
        ->get([
            'expenses.*',            
        ]);

        return view('expenses.query.index', compact('expenses_pend', 'expenses_aprov'));   
    }

    public function aprove(Expense $expense)
    {
        $expense->pendente = 0;
        $expense->useraprova_id = Auth::user()->id;
        $expense->save();

        return redirect()->route('query');
    }

    public function detail() 
    {
        $expense_id = Request()->id;

        $expenseItems = ExpenseItem::join('category_expenses', 'expense_items.category_expenses_id', '=', 'category_expenses.id')
                                   ->where('expense_items.expenses_id', $expense_id)
                                   ->get([
                                       'expense_items.*',
                                       'category_expenses.description'
                                   ]);

        return json_encode($expenseItems);
    }
}
