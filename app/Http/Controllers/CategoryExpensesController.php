<?php

namespace App\Http\Controllers;

use App\CategoryExpenses;
use App\ExpenseItem;
use Illuminate\Http\Request;

class CategoryExpensesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoryExpenses = CategoryExpenses::orderBy('description')->get();
        return view('registrations.categoriesExpenses.index', compact('categoryExpenses'));    
    }

    public function list() 
    {
        $categoryExpenses = CategoryExpenses::orderBy('description')->get(['id','description']);
        return json_encode($categoryExpenses);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('registrations.categoriesExpenses.create');        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->_validate($request);
        CategoryExpenses::create($data);
        return redirect()->route('categoria-de-despesas.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryExpenses  $categoryExpenses
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryExpenses $categoryExpenses)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryExpenses  $categoryExpenses
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryExpenses $categoria_de_despesa)
    {
        return view('registrations.categoriesExpenses.edit', compact('categoria_de_despesa'));          
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryExpenses  $categoryExpenses
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryExpenses $categoria_de_despesa)
    {
        $data = $this->_validate($request);
        $categoria_de_despesa->fill($data);
        $categoria_de_despesa->save();
        return redirect()->route('categoria-de-despesas.index');        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryExpenses  $categoryExpenses
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryExpenses $categoria_de_despesa)
    {
        $expenseItems = ExpenseItem::where('category_expenses_id', $categoria_de_despesa->id)->count();
        if ($expenseItems > 0) {
            return redirect()->route('categoria-de-despesas.index')->withErrors(['Categoria de despesa em uso!']);
        }

        if ($categoria_de_despesa->delete()) {
            return redirect()->route('categoria-de-despesas.index');
        }
    }

    protected function _validate(Request $request)
    {
        $rules = [
            'description' => 'required',        
        ];
        $messages = [
            'description.required' => 'Informe a descrição!',                 
        ];
        return $this->validate($request, $rules, $messages);
    }
}
