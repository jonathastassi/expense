<?php

namespace App\Http\Controllers;

use App\CentersCost;
use App\Expense;
use App\User;
use Illuminate\Http\Request;

class CentersCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $centersCosts = CentersCost::orderBy('description')->get();
        return view('registrations.costsCenter.index', compact('centersCosts'));     
    }

    public function list() 
    {
        $centersCosts = CentersCost::orderBy('description')->get(['id','description']);
        return json_encode($centersCosts);        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::where('isAdmin', 1)
                     ->orderBy('name')
                     ->get();
        return view('registrations.costsCenter.create', compact('users'));             
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->_validate($request);
        CentersCost::create($data);
        return redirect()->route('centro-de-custos.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CentersCost  $centersCost
     * @return \Illuminate\Http\Response
     */
    public function show(CentersCost $centersCost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CentersCost  $centersCost
     * @return \Illuminate\Http\Response
     */
    public function edit(CentersCost $centro_de_custo)
    {
        $users = User::where('isAdmin', 1)
                     ->orderBy('name')
                     ->get();

        return view('registrations.costsCenter.edit', compact('centro_de_custo', 'users'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CentersCost  $centersCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CentersCost $centro_de_custo)
    {
        $data = $this->_validate($request);
        $centro_de_custo->fill($data);
        $centro_de_custo->save();
        return redirect()->route('centro-de-custos.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CentersCost  $centersCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(CentersCost $centro_de_custo)
    {
        $centercost = Expense::where('centercost_id', $centro_de_custo->id)->count();
        if ($centercost > 0) {
            return redirect()->route('centro-de-custos.index')->withErrors(['Centro de custo em uso!']);
        }

        if ($centro_de_custo->delete()) {
            return redirect()->route('centro-de-custos.index');
        }
    }

    protected function _validate(Request $request)
    {
        $rules = [
            'description' => 'required',        
            'user_id' => 'required',        
        ];
        $messages = [
            'description.required' => 'Informe a descrição!',                 
            'user_id.required' => 'Informe o usuário aprovador!',                 
        ];
        return $this->validate($request, $rules, $messages);
    }

}
