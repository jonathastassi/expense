<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::orderBy('name')->get();
        return view('registrations.users.index', compact('users'));     
    }
    
    public function setAdmin(Request $request, User $usuario)
    {
        $usuario->isAdmin = !$usuario->isAdmin;
        $usuario->save();
        return redirect()->route('usuarios.index');
    }

}
