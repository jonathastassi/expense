<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CentersCost extends Model
{
    protected $fillable = ['description', 'user_id'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
