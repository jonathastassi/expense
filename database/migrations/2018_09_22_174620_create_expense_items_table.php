<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_items', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('valor');
            $table->integer('category_expenses_id')->unsigned();
            $table->foreign('category_expenses_id')->references('id')->on('category_expenses');   
            $table->integer('expenses_id')->unsigned();
            $table->foreign('expenses_id')->references('id')->on('expenses');      
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_items');
    }
}
